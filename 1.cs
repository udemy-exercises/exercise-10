using System;

class Exercise10
{
   static void Main()
   {
      var person1 = new Person();
      var person2 = new Person();

      person1.name = Console.ReadLine();
      person1.age = int.Parse(Console.ReadLine());      

      person2.name = Console.ReadLine();
      person2.age = int.Parse(Console.ReadLine());

      Console.WriteLine("Dados da primeira pessoa:");
      Console.WriteLine($"Nome: {person1.name}");
      Console.WriteLine($"Idade: {person1.age}");
     
      Console.WriteLine();

      Console.WriteLine("Dados da segunda pessoa:");
      Console.WriteLine($"Nome: {person2.name}");
      Console.WriteLine($"Idade: {person2.age}");

      if (person1.age > person2.age)
      {
         Console.WriteLine($"Pessoa mais velha: {person1.name}");
      }

      else
      {
         Console.WriteLine($"Pessoa mais velha: {person2.name}");
      }
   }
}

class Person
{
   public string name;
   public int age;
}
