using System;

class Exercise10
{
   static void Main()
   {
      var worker1 = new Employee();
      var worker2 = new Employee();

      worker1.name = Console.ReadLine();
      worker1.salary = double.Parse(Console.ReadLine());      

      worker2.name = Console.ReadLine();
      worker2.salary = double.Parse(Console.ReadLine());

      Console.WriteLine("Dados do primerio funcionario");
      Console.WriteLine($"Nome: {worker1.name}");
      Console.WriteLine($"Salario: {worker1.salary}");      

      Console.WriteLine();

      Console.WriteLine("Dados do segundo funcionario");
      Console.WriteLine($"Nome: {worker2.name}");
      Console.WriteLine($"Salario: {worker2.salary}");

      double averageSalary = (worker1.salary + worker2.salary) / 2;
      Console.WriteLine($"Salario medio = {averageSalary}");
   }
}

class Employee
{
   public string name;
   public double salary;
}
